function sim(n, doors, switchornot) {
    var d1 = doors[Math.floor(Math.random() * 3)];
    var win = switchornot ? !d1 : d1;
    if (n == 0) {
        return win ? 1 : 0;
    }
    else {
        return (win ? 1 : 0) + sim(n - 1, doors, switchornot);
    }
}
var wins = sim(3000, [true, false, false], true);
console.log('Wins: ' + wins.toString());
console.log('Losses: ' + (3000 - wins).toString());

function sim(n: number, doors: boolean[], switchornot: boolean): number {
    let d1 = doors[Math.floor(Math.random() * 3)];
    let win = switchornot ? !d1 : d1;

    if (n == 0) {
        return win ? 1 : 0;
    } else {
        return (win ? 1 : 0) + sim(n - 1, doors, switchornot);
    }
}

let wins = sim(3000, [true, false, false], true);
console.log('Wins: ' + wins.toString());
console.log('Losses: ' + (3000 - wins).toString());

val initdoors = [true, false, false]
val seed = Random.newgen ()

fun sim doors switch 0 = []
  | sim doors switch n =
      let val door1 = List.nth (doors, Random.range (0, 3) seed)
      in (if switch then not door1 else door1) :: sim doors switch (n - 1)
      end

fun wins [] = 0
  | wins (false :: outcomes) = wins outcomes
  | wins (true :: outcomes)  = 1 + wins outcomes

fun run n switch =
  let val outcomes = sim initdoors switch n
      val ws = wins outcomes
  in (print ("Wins: " ^ Int.toString ws ^ "\n");
      print ("Losses: " ^ Int.toString (n - ws) ^ "\n"))
  end

val _ = run 3000 true

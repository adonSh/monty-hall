val initdoors = (true, false, false)
val _ = MLton.Random.srand (valOf (MLton.Random.seed ()))

fun pick (d1, d2, d3) 0 = d1
  | pick (d1, d2, d3) 1 = d2
  | pick (d1, d2, d3) 2 = d3
  | pick _ _ = let exception Won'tHappen in raise Won'tHappen end

fun sim doors switch 0 = []
  | sim doors switch n =
      let val door1 = pick doors (Char.ord (MLton.Random.alphaNumChar ()) mod 3)
      in (if switch then not door1 else door1) :: sim doors switch (n - 1)
      end

fun run n switch =
  let val outcomes = sim initdoors switch n
      val wins = foldr (fn (win, z) => if win then 1 + z else z) 0 outcomes
  in (print ("Wins: " ^ Int.toString wins ^ "\n");
     print ("Losses: " ^ Int.toString (n - wins) ^ "\n"))
  end

val _ = run 3000 true

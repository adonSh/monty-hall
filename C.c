#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
        (void) argc;
        (void) argv;
        int initdoors[3] = { 1, 0, 0 };
        int switchornot = 1;
        int wins = 0;
        int initial = 0;
        int isawin = 0;

        srand(time(NULL));
        for (int i = 0; i < 3000; i++) {
                initial = initdoors[rand() % 3];
                isawin = initial;

                if (switchornot)
                        isawin = !initial;
                if (isawin)
                        wins++;
        }

        printf("Wins: %d\nLosses: %d\n", wins, 3000 - wins);

        return 0;
}

import java.util.Random

fun main(args: Array<String>) {
    val initdoors = listOf(true, false, false)
    val switch = true
    var wins = 0
    val r = Random()

    for (i in 1..3000) {
        val initial = initdoors[r.nextInt(3)]
        var isawin = initial

        if (switch) {
            isawin = !initial
        }
        if (isawin) {
            wins++;
        }
    }
    println("Winz: $wins")
    println("Losses: ${3000 - wins}")
}

(define initdoors '(#t #f #f))

(define (pick n xs)
  (if (= 0 n)
    (car xs)
    (pick (- n 1) (cdr xs))))

(define (sim doors switch n)
  (if (= 0 n)
    '()
    (let ((door1 (pick (random 3) doors)))
      (cons (if switch (not door1) door1) (sim doors switch (- n 1))))))

(define (run switch n)
  (let* ((outcomes (sim initdoors switch n))
         (wins (fold (lambda (w z) (if w (+ 1 z) z)) 0 outcomes)))
    (display "Wins: ") (display wins) (display "\n")
    (display "Losses: ") (display (- n wins)) (display "\n")))

(run #t 3000)

local initdoors = { true, false, false }
local switch = true
local wins = 0

math.randomseed(os.time())
for i=1, 3000 do
  local initial = initdoors[math.random(1,3)]
  local win = initial

  if switch then
    win = not initial
  end

  if win then
    wins = wins + 1
  end
end

print('Wins: ' .. tostring(wins) .. '\nLosses: ' .. tostring(3000 - wins))

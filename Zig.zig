const std = @import("std");

pub fn main() !void {
    const stdout = &(try std.io.getStdOut()).outStream().stream;
    const doors = [3]bool{true, false, false};
    const switchornot = true;
    var wins: i32 = 0;
    var r = std.rand.DefaultPrng.init(std.time.milliTimestamp());

    var i: i32 = 0;
    while (i < 3000) : (i += 1) {
        var isawin = doors[r.random.int(u64) % 3];

        if (switchornot) {
            isawin = !isawin;
        }
        if (isawin) {
            wins += 1;
        }
    }

    try stdout.print("Wins: {}\nLosses: {}\n", wins, 3000 - wins);
}

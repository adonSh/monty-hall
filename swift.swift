import Foundation

let initdoors = [ true, false, false ]
let switchornot = true
var wins = 0

for _ in 1...3000 {
        let initial = initdoors[Int(arc4random()) % 3]
        var isawin = initial

        if switchornot {
                isawin = !initial
        }
        if isawin {
                wins = wins + 1
        }
}

print("Wins: " + String(wins))
print("Losses: " + String(3000 - wins))

var doors = [true, false, false];
var switchornot = true;
var wins = 0;
for (var i = 0; i < 3000; i++) {
    var initial = doors[Math.floor(Math.random() * 3)];
    var win = initial;
    if (switchornot) {
        win = !initial;
    }
    if (win) {
        wins = wins + 1;
    }
}
console.log('Wins: ' + wins.toString());
console.log('Losses: ' + (3000 - wins).toString());

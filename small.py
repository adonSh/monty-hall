import random

initdoors = (True, False, False)
switch = True
wins = 0

for _ in range(3000):
    initial = initdoors[random.randint(0, 2)]
    win = not initial if switch else initial
    if win:
        wins += 1

print('Wins:' + str(wins))
print('Losses:' + str(3000 - wins))

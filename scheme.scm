(define initdoors '(#t #f #f))

(define (pick n xs)
  (if (= 0 n)
    (car xs)
    (pick (- n 1) (cdr xs))))

(define (sim doors switch n)
  (if (= 0 n)
    '()
    (let ((door1 (pick (random 3) doors)))
      (cons (if switch (not door1) door1) (sim doors switch (- n 1))))))

(define (wins outcomes)
  (if (null? outcomes)
    0
    (if (car outcomes)
      (+ 1 (wins (cdr outcomes)))
      (wins (cdr outcomes)))))

(define (run switch n)
  (let* ((outcomes (sim initdoors switch n)) (ws (wins outcomes)))
    (display "Wins: ") (display ws) (display "\n")
    (display "Losses: ") (display (- n ws)) (display "\n")))

(run #t 3000)

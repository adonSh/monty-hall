let doors = [ true, false, false ];
let switchornot = true;
let wins = 0;

for (let i = 0; i < 3000; i++) {
    let initial = doors[Math.floor(Math.random() * 3)];
    let win = initial;

    if (switchornot) {
        win = !initial;
    }

    if (win) {
        wins = wins + 1;
    }
}

console.log('Wins: ' + wins.toString());
console.log('Losses: ' + (3000 - wins).toString());

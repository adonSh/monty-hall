val initdoors = [true, false, false]
val seed = Random.rand (1, 1)

fun sim doors switch 0 = []
  | sim doors switch n =
      let val door1 = List.nth (doors, (Random.randRange (0, 2) seed))
      in (if switch then not door1 else door1) :: sim doors switch (n - 1)
      end

fun run n switch =
  let val outcomes = sim initdoors switch n
      val wins = foldr (fn (win, z) => if win then 1 + z else z) 0 outcomes
  in (print ("Wins: " ^ Int.toString wins ^ "\n");
      print ("Losses: " ^ Int.toString (n - wins) ^ "\n"))
  end

val _ = run 3000 true

import java.util.Random;

public class Monty {
    public static void main(String[] args) {
        boolean initdoors[] = { true, false, false };
        boolean switchornot = true;
        int wins = 0;
        boolean initial = false;
        boolean isawin = false;

        Random r = new Random();
        for (int i = 0; i < 3000; i++) {
            initial = initdoors[r.nextInt(3)];
            isawin = initial;

            if (switchornot) {
                isawin = !initial;
            }
            if (isawin) {
                wins++;
            }
        }

        System.out.println("Wins: " + wins);
        System.out.println("Losses: " + (3000 - wins));
    }
}

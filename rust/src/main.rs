extern crate rand;

use rand::Rng;

fn main() {
    let initdoors = [true, false, false];
    let switchornot = true;
    let mut wins = 0;

    for _ in 0..3000 {
        let initial = initdoors[rand::thread_rng().gen_range(0, 3)];
        let mut isawin = initial;

        if switchornot {
            isawin = !initial;
        }
        if isawin {
            wins = wins + 1;
        }
    }
    println!("Wins: {}\nLosses: {}", wins, 3000 - wins);
}

val seed = Random.newgen ()

fun sim n doors switch =
      let val d1  = List.nth (doors, Random.range (0, 3) seed)
          val win = if switch then not d1 else d1
      in 
        if n = 0 then
          if win then 1 else 0
        else
          (if win then 1 else 0) + (sim (n - 1) doors switch)
      end

val _ = let val wins = sim 3000 [true, false, false] true
        in (print ("Wins: " ^ Int.toString wins ^ "\n");
            print ("Losses: " ^ Int.toString (3000 - wins) ^ "\n"))
        end

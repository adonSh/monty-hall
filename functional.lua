local function sim(n, doors, switch)
  local d1 = doors[math.random(1, 3)]
  local win = not switch and d1 or not d1 

  if n == 0 then
    return win and 1 or 0
  else
    return (win and 1 or 0) + sim(n - 1, doors, switch)
  end
end

math.randomseed(os.time())
local wins = sim(3000, {true, false, false}, true)
print('Wins: ' .. tostring(wins) .. '\nLosses: ' .. tostring(3000 - wins))

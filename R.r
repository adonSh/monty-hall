sim <- function(n, doors, switch) {
    d1 <- doors[sample(1:3, 1)]
    win <- if (switch) { !d1 } else { d1 }

    if (n == 0) {
        if (win) { 1 } else { 0 }
    } else {
        (if (win) { 1 } else { 0 }) + sim(n - 1, doors, switch)
    }
}

wins <- sim(300, c(TRUE, FALSE, FALSE), TRUE)
cat(paste('Wins: ', as.character(wins), '\n'))
cat(paste('Losses: ', as.character(300 - wins), '\n'))

const std = @import("std");

const ctime = @cImport(@cInclude("time.h"));
const cstd = @cImport(@cInclude("stdlib.h"));

pub fn main() !void {
    const stdout = &(try std.io.getStdOut()).outStream().stream;
    const doors = [3]bool{true, false, false};
    const switchornot = true;
    var wins: i32 = 0;

    cstd.srand(@intCast(u32, ctime.time(0)));
    var i: i32 = 0;
    while (i < 3000) : (i += 1) {
        const initial = doors[@intCast(u32, cstd.rand()) % u32(3)];
        var isawin = initial;

        if (switchornot) {
            isawin = !initial;
        }
        if (isawin) {
            wins = wins + 1;
        }
    }

    try stdout.print("Wins: {}\nLosses: {}\n", wins, 3000 - wins);
}

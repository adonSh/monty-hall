package main

import (
    "fmt"
    "math/rand"
    "time"
)

func main() {
    initdoors := [3]bool{true, false, false}
    switchornot := true
    wins := 0

    rand.Seed(time.Now().UnixNano())
    for i := 1; i < 3000; i++ {
        initial := initdoors[rand.Intn(3)]
        isawin := initial

        if switchornot {
            isawin = !initial
        }
        if isawin {
            wins++
        }
    }

    fmt.Printf("Wins: %d\nLosses: %d\n", wins, 3000 - wins)
}

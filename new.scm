;; returns the nth element of the list xs
(define (pick n xs)
  (if (= n 0)
    (car xs)
    (pick (- n 1) (cdr xs))))

;; returns the number of wins after n tries
(define (sim n doors switch)
  (let* ((d1 (pick (random 3) doors)) (win (if switch (not d1) d1)))
    (if (= n 0)
      (if win 1 0)
      (+ (if win 1 0) (sim (- n 1) doors switch)))))

;; run the simulation and print the results
(let ((wins (sim 3000 '(#t #f #f) #t)))
  (display "Wins: ") (display wins) (display "\n")
  (display "Losses: ") (display (- 3000 wins)) (display "\n"))

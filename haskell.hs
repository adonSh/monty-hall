import System.Random

sim :: Int -> [Bool] -> Bool -> IO Int
sim 1 doors switch = rn >>= \i ->
    let win = if switch then not (doors !! i) else doors !! i
    in return (if win then 1 else 0)
sim n doors switch = rn >>= \i ->
    let win = if switch then not (doors !! i) else doors !! i
--  in fmap ((if win then 1 else 0) +) (sim (n - 1) doors switch)
    in sim (n - 1) doors switch >>= \sum -> return (sum + if win then 1 else 0)

rn :: IO Int
rn = System.Random.newStdGen >>= \g ->
    return (fst (System.Random.randomR (0, 2) g))

main :: IO ()
--main = do
--    let n = 3000
--    wins <- sim n [True, False, False] True
--    putStrLn ("Wins: " ++ (show wins))
--    putStrLn ("Losses: " ++ (show (n - wins)))

main = let n = 3000 in sim n [True, False, False] True >>= \wins ->
    putStrLn ("Wins: " ++ (show wins) ++ "\nLosses: " ++ (show (n - wins)))
